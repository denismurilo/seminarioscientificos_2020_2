package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String email;
    private String nome;
    private Double salario;
    private String telefone;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

    public Professor(Instituicao instituicao) {
        super();
        this.instituicao = instituicao;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    // public void setInstituicao(Instituicao instituicao) {
    // this.instituicao = instituicao;
    // }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    // public void setSeminarios(List<Seminario> seminarios) {
    // this.seminarios = seminarios;
    // }

    public void adicionarSeminario(Seminario seminario) {
        this.seminarios.add(seminario);
    }

    public boolean possuiSeminario(Seminario seminario) {
        return this.seminarios.contains(seminario);
    }

}
