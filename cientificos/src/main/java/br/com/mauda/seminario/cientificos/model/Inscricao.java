package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Boolean direitoMaterial;
    private SituacaoInscricaoEnum situacao;
    private Seminario seminario;
    private Estudante estudante;

    public Inscricao(Seminario seminario) {
        super();
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
        // AJUSTAR

        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    // public void setSeminario(Seminario seminario) {
    // this.seminario = seminario;
    // }

    public Estudante getEstudante() {
        return this.estudante;
    }

    // public void setEstudante(Estudante estudante) {
    // this.estudante = estudante;
    // }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    // public void setSituacao(SituacaoInscricaoEnum situacao) {
    // this.situacao = situacao;
    // }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        estudante.adicionarInscricao(this);
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante = null;
        this.direitoMaterial = null;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

}
